//
//  ViewController.swift
//  Miracle Pills
//
//  Created by Nuwan on 2016/10/03.
//  Copyright © 2016 Nuwan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var statePicker: UIPickerView!
    @IBOutlet weak var statePickerBtn: UIButton!
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var zipTxt: UITextField!
    @IBOutlet weak var buynowBtn: UIButton!
    @IBOutlet weak var sucessImg: UIImageView!
    
    let states = ["Central","Eastern"," North Central","Northern","North Western"," Sabaragamuwa","Southern","Uva","Western"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        statePicker.dataSource = self
        statePicker.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

  
    @IBAction func statePickerBtnPressed(_ sender: AnyObject) {
        if statePicker.isHidden == true{
            statePicker.isHidden = false
        }
        countryTxt.isHidden = true
        countryLabel.isHidden = true
        zipTxt.isHidden = true
        zipLabel.isHidden = true
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        statePickerBtn.setTitle(states[row], for: UIControlState.normal)
        if statePicker.isHidden == false {
            statePicker.isHidden = true
            countryTxt.isHidden = false
            countryLabel.isHidden = false
            zipTxt.isHidden = false
            zipLabel.isHidden = false
        }
        }
        
  
    @IBAction func buynowPressed(_ sender: AnyObject) {
        for i in 1...18 {
            self.view.viewWithTag(i)?.isHidden = true
        }
        sucessImg.isHidden = false
        

    }

}

